const { Vehicle } = require('../models')

const createVehicle = async(req, res) => {
    const { name, price, capacity } = req.body
        // req.body.name, req.body.price, req.body.quantity
    try {
        // untuk dapat extension file nya
        const split = req.file.originalname.split('.')
        const ext = split[split.length - 1]

        // upload file ke imagekit
        const img = await imagekit.upload({
            file: req.file.buffer, //required
            fileName: `${req.file.originalname}.${ext}`, //required
        })
        console.log(img.url)

        const newVehicle = await Vehicle.create({
            name,
            price,
            capacity,
        })

        res.status(201).json({
            status: 'success',
            data: {
                newVehicle
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const findVehicle = async(req, res) => {
    try {
        const vehicle = await Vehicle.findAll()
        res.status(200).json({
            status: 'Success',
            data: {
                vehicle
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const findVehicleById = async(req, res) => {
    try {
        const vehicle = await Vehicle.findOne({
            where: {
                id: req.params.id
            }
        })
        res.status(200).json({
            status: 'Success',
            data: {
                vehicle
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const editVehicle = async(req, res) => {
    try {
        const { name, price, capacity } = req.body
        const id = req.params.id
        const vehicle = await Vehicle.update({
            name,
            price,
            capacity,
        }, {
            where: {
                id
            }
        })
        res.status(200).json({
            status: 'Success',
            data: {
                id,
                name,
                price,
                capacity,
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const deleteVehicle = async(req, res) => {
    try {
        const id = req.params.id
        await Vehicle.destroy({
            where: {
                id
            }
        })

        res.status(200).json({
            status: 'success',
            message: `Kendaraan dengan id ${id} telah dihapus`
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

module.exports = {
    createVehicle,
    findVehicle,
    findVehicleById,
    editVehicle,
    deleteVehicle,
}