const router = require('express').Router()
const Vehicle = require('../controller/vehicleController')
const Admin = require('../controller/admin/vehicleController')

// admin client side
router.get('/admin', Admin.homepage)
router.get('/admin/vehicles', Admin.dataPage)
router.get('/admin/add', Admin.createPage)
router.post('/admin/add', Admin.createVehicle)
router.get('/admin/edit/:id', Admin.editPage)
router.post('/admin/edit/:id', Admin.editVehicle)
router.post('/admin/delete/:id', Admin.deleteVehicle)

// API server
router.post('/api/vehicle', Vehicle.createVehicle)
router.get('/api/vehicle', Vehicle.findVehicle)
router.get('/api/vehicle/:id', Vehicle.findVehicleById)
router.put('/api/vehicle/:id', Vehicle.editVehicle)
router.delete('/api/vehicle/:id', Vehicle.deleteVehicle)

// detail/search page
router.get('/admin/vehicle/:name/:id', Admin.detailPage)

module.exports = router